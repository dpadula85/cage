Validation
==========

- `MM_opt` : run the vmd script compare.vmd to superimpose QM and MM minimised
geometries

- `MD_vacuo`: 500 ps trajectory _in vacuo_.

- force field in the `joyce.last.top` file
